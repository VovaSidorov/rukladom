<?php
    /*plugins | 160.by @7*/
    include ('includes/application_top.php');

    if (isset($_GET['page']) &&  !empty($_GET['page']) or (isset($_GET['main_page']) && !empty($_GET['main_page'])) )
    {
        if (isset(p::$action['page'][$_GET['page']]) && isset(p::$action_plug[$_GET['page']]) )
        {		
            $_plug_name = p::$action_plug[$_GET['page']];	
            p::$name = $_plug_name;
            p::set_dir();
            $_page = $_GET['page'];

            $_param = explode('::', $_page); 

            if ( count( $_param ) == 2 )
            {
                if ( class_exists ($_param[0]))
                {
                    if ( method_exists($_param[0], $_param[1]))
                    {
						$result = call_user_func(array( $_param[0], $_param[1]));
                    }
                }
            }
            else
            {
                if (function_exists($_page))
                {
                    $_page(); 
                }
            }


            exit();
        }         

        if (isset(p::$action['main'][$_GET['main_page']]) and isset(p::$action_plug[$_GET['main_page']]))
        {
            $m_content = '';
            $_param = explode('::', $_GET['main_page']);

            if ( count( $_param ) == 2 )
            {
                if ( class_exists ($_param[0]))
                {
                    if ( method_exists($_param[0], $_param[1]))
                    {
                        require ('includes/header.php'); 
                        
                        p::$name = p::$action_plug[$_GET['main_page']];   
                        p::set_dir();
                        
                        ob_start();
						$result = call_user_func(array( $_param[0], $_param[1]));
                        $m_content = ob_get_contents();
                        ob_end_clean();
                    }
                }
            }
            else
            {
                if (function_exists($_tag))
                {
                    require ('includes/header.php');
                    p::$name = p::$action_plug[$_GET['main_page']];   
                    p::set_dir();

                    ob_start();
                    $_GET['main_page'](); 
                    $m_content = ob_get_contents();
                    ob_end_clean();
                }
            }

            if ( !empty($m_content) )
            { 
                $vamTemplate->assign('CONTENT_BODY', $m_content);

                $_array = array('img' => 'button_back.gif', 
                    'href' => 'javascript:history.back(1)', 
                    'alt' => IMAGE_BUTTON_BACK,								
                    'code' => ''
                );

                $vamTemplate->assign('language', $_SESSION['language']);
                $main_content = $vamTemplate->fetch(CURRENT_TEMPLATE.'/module/content.html');

                $vamTemplate->assign('language', $_SESSION['language']);
                $vamTemplate->assign('main_content', $main_content);


                $template = CURRENT_TEMPLATE.'/index.html';

                $vamTemplate->display($template);
            }
        }
        die('404!');
    }
	/*plugins | 160.by @7*/
?>