<?php
    /*
    Plugin Name: Отладка магазина
    Plugin URI: http://160.by/
    Version: 1.1
    Author: Евгений Фоминов
    Author URI: http://160.by/
    */		

    add_action('application_bottom', 'debug::run');
    add_action('db_query', 'debug::add');

    class debug
    {
        public function add($v)
        {          
            if (!isset( p::$var['debug']))  p::$var['debug'] = array();
            p::$var['debug'][] = array('query'=>$v['query'], 'time'=>$v['time']);    
        }

        public function run()
        {
            $db_query =   p::$var['debug'];
            if ( function_exists('memory_get_usage') )
            {
                echo  '<center>mem: '.round(memory_get_usage()/1024/1024, 2) . 'MB, db: '.count($db_query).'</center>';
            }

            echo "<center><div style='overflow: scroll; width: 60%; height: 200px; text-align: left;border: 1px dotted blue;'>";
            
            if ( count(p::$var['debug']) > 0 )
            {
                foreach ( $db_query as $v1 => $v2)
                {
                    $db_query[$v1]['query'] = str_replace('select', '<font color="#ff1493">select</font>', $db_query[$v1]['query'] );
                    $db_query[$v1]['query'] = str_replace('from', '<font color="#ff1493">from</font>', $db_query[$v1]['query'] );
                    $db_query[$v1]['query'] = str_replace('where', '<font color="#ff1493">where</font>', $db_query[$v1]['query'] );
                }
            }


            // echo count($db_query).'<br>';
            foreach ($db_query as $v1 => $v2)
            {
                //echo $v2['query']."\n"."\n"; 
                $time = $v2['time'];
                $time  = 10* $time;
                $time = round($time, 4);
                echo  '<font color="red">('.$time.')</font> <font color="blue">'.$v2['query']."</font><br />";
            }

            echo "</div></center><br /><br />";
        }
    }

?>