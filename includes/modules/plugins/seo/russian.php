<?php
  $lang['seo::generator'] = 'Генератор SEO URL';
  $lang['seo::redirect'] = 'Редактор редиректов';
  $lang['seo_news'] = 'ЧПУ для новостей';
  $lang['seo_type_pro'] = 'ЧПУ для товаров';
  $lang['seo_type_pro_1'] = 'cat_name/name/';
  $lang['seo_type_pro_2'] = 'cat_name/name.html';
  $lang['seo_type_pro_3'] = 'name.html';  
  
  $lang['seo_type_cat'] = 'ЧПУ для категорий';
  $lang['seo_type_cat_1'] = 'cat1/cat2/';
  $lang['seo_type_cat_2'] = 'cat1/cat2.html';
  $lang['seo_type_cat_3'] = 'cat.html';
  $lang['seo_redirect_desc'] = 'Создать переадресацию со старых адресов';
?>