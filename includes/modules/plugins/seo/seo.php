<?php
    /*
    Plugin Name: ЧПУ
    Plugin URI: http://160.by/
    Version: 1.1
    Author: Евгений Фоминов
    Author URI: http://160.by/
    */  
   add_action('page', 'seo::go');
   add_action('main_page_admin', 'seo::generator');
   add_action('main_page_admin', 'seo::redirect');
   add_action('main_page_admin', 'seo::remove_all');
   add_action('insert_category', 'seo::insert_category');
   add_action('remove_categories', 'seo::remove_categories');
   add_action('insert_product', 'seo::insert_product');
   add_action('remove_product', 'seo::remove_product');
   add_action('insert_news', 'seo::insert_news');
   add_action('remove_news', 'seo::remove_news');
   add_filter('menu_modules', 'seo::menu');
   
   include_once('seo.func.php');
   
   class seo
   {	  
      static $url = '';
	  
	  public static function generator()
	  {
	      include('seo.gen.php');
	  }		  
	  
	  public static function redirect()
	  {
	      include('seo.red.php');
	  }		  
	  
	  public static function menu($ar)
	  {
	      $ar[] =  array(
                'code' => 'seo',
                'title' => 'Редиректы',
                'link' => 'plugins.php?main_page=seo::redirect'
            ); 
		  
		  return $ar;
	  }		  
      
	  public static function check_double($url)
	  {

	     if ( isset( seo::$url[ $url ] ) ) 
		 {  
			   $i=1;
			   $u = $url;

			   while ( isset( seo::$url[ $u ] ) )
			   {
			      if (substr($url, strlen($url)-1)  == '/')
	              {
		             $url2= substr($url, 0,strlen($url)-1);
					 $u = $url2.'-'.$i.'/';
	              }
				  else
				  {
				     $_tmp = explode('.', $url);
					 if ( is_array($_tmp)&& $_tmp[ count($_tmp)-1] == 'html')
					 {
					    unset( $_tmp[ count($_tmp)-1]);
						$url2 = implode('.', $_tmp);
						$u = $url2.'-'.$i.'.html'; 
					 }
					 else
					 {
				        $url2 = $url;
					 }
				  }

			      
                  $i++;				  
			   }
			   $url = $u;
	     }
	     return $url;
	  }
	  
	  public static function remove_all()
	  {
	     p::query("delete from `seo`");	
	     p::query("update categories set categories_url=''"); 
         p::query("update products set products_page_url=''"); 		 
         p::query("update latest_news set news_page_url=''");
         echo 'Удалено';		 
      }
	  
	  public static function insert_category($ar)
	  {
		 $cat_id = (int)$ar['categories_id'];
		 $name = $ar['categories_name'][1];
		 $url = $ar['categories_url'];
		
         p::query("delete from `seo` where `query`='index.php?cat=".$cat_id."'");		
		 seo::get_seo();
		 
		 $al = seo::get_key($cat_id);
		 if ( is_array($al) && count($al) > 0){
		    foreach ($al as $_id => $name){
			   $al[$_id] = cleaner( $name );
			}

			if ( get_option('seo_type_cat') == 'seo_type_cat_1')
            {			
			   $url2 = implode('/', $al).'/';
			}
			elseif ( get_option('seo_type_cat') == 'seo_type_cat_2')
			{
			   $url2 = implode('/', $al).'.html';
			}elseif ( get_option('seo_type_cat')== 'seo_type_cat_3')
			{
			   $url2 =  cleaner( $name ).'.html';
			}
			
			$url2 = seo::check_double($url2);
		 }
		 
		 if (  empty( $url ) && !empty($name) ) $url = $url2;
		 
		 if (!empty ( $url ) )
		 {
		    $url = seo::check_double($url);
		    p::query("update categories set categories_url='".$url."' where categories_id='".$cat_id."'"); 			
		    p::query("insert into `seo` (`query`, `keyword`) VALUES ('index.php?cat=".$cat_id."', '".$url."');");

			if ($ar['url'] != $url2 && !empty($ar['url']) && isset($ar['gen']) && get_option('seo_redirect')=='true')
			{
		         p::query("insert into `seo` (`query`, `keyword`, `type`) VALUES ('".$url2."', '".$ar['url']."', '301');");
			}
		 }
		 
		 return $url;
	  }		  
	 
	  // добавление чпу для новости
	  public static function insert_news($ar)
	  { 
		 
		 $news_id = (int)$ar['news_id'];
		 $name = $ar['headline'];
		 $url = $ar['news_page_url'];
		 
		 p::query("delete from `seo` where `query`='news.php?news_id=".$news_id."'");	
		 seo::get_seo();
		 
		 if (  empty( $url ) && !empty($name) ) {
		    $n = cleaner( $name );
		    $url2 = get_option('seo_news');
			$url2 = str_replace('%name%', $n, $url2);
			$url2 = seo::check_double($url2);
		 }
		 
		 if (  empty( $url ) && !empty($name) ) $url = $url2;
		 
		 if (!empty ( $url ) )
		 {
		    $url = seo::check_double($url);
		    p::query("update latest_news set news_page_url='".$url."' where news_id='".$news_id."'"); 
		    p::query("insert into `seo` (`query`, `keyword`) VALUES ('news.php?news_id=".$news_id."', '".$url."');");
			
			if ($ar['news_page_url'] != $url2 && !empty($ar['news_page_url']) && isset($ar['gen'])  && get_option('seo_redirect')=='true')
			{
			   p::query("insert into `seo` (`query`, `keyword`, `type`) VALUES ('".$url2."', '".$ar['news_page_url']."', '301');");
			}
		 }
		 
		 return $url;
	  }
	  
	  public static function insert_product($ar)
	  {

	     $id = (int)$ar['products_id'];
		 $url = $ar['products_page_url'];
		 $name = $ar['products_name'][1];

		 p::query("delete from `seo` where `query`='product_info.php?products_id=".$id."'");
		 seo::get_seo();
					
		 $cat_id = seo::cat_id($id);
		 $al = seo::get_key($cat_id);
		 $al[] = $name;
		 $url2 = '';
		 
         if ( is_array($al) && count($al) > 0){
		    foreach ($al as $_id => $name){
			   $al[$_id] = cleaner( $name );
			}

			if ( get_option('seo_type_pro') == 'seo_type_pro_1')
            {			
			   $url2 = implode('/', $al).'/';
			}
			elseif ( get_option('seo_type_pro') == 'seo_type_pro_2')
			{
			   $url2 = implode('/', $al).'.html';
			}elseif ( get_option('seo_type_pro')== 'seo_type_pro_3')
			{
			   $url2 =  cleaner( $name ).'.html';
			}
			
			$url2 = seo::check_double($url2);
		 }
		 
		 if (  empty( $url ) && !empty($name) )  $url = $url2;
		 
         if ( !empty( $url ) ) {
		     $url = seo::check_double($url);
		    p::query("update products set products_page_url='".$url."' where products_id='".$id."'"); 
		    p::query("insert INTO `seo` (`query`, `keyword`) VALUES ('product_info.php?products_id=".$id."', '".$url."');");
			
			if ($ar['url'] != $url2 && !empty($ar['url']) && isset($ar['gen']) && get_option('seo_redirect')=='true')
			{
			   p::query("insert into `seo` (`query`, `keyword`, `type`) VALUES ('".$url2."', '".$ar['url']."', '301');");
			}   
         }
		 
		 return $url;
	  }	 
	  
	  public static function cat_id($products_id)
	  {
	      $a = p::query('select * from products_to_categories where products_id='.(int)$products_id);
		  $b = p::fetch_array($a, true);
		  return $b['categories_id'];	  
	  }
	  
      public static function get_key($cat_id)
      {
	      $categories_query = "select c.categories_id,
                                           cd.categories_name,
                                           c.categories_image,
										    c.sort_order,
                                           c.parent_id from ".TABLE_CATEGORIES." c, ".TABLE_CATEGORIES_DESCRIPTION." cd
                                           where c.categories_status = '1'
                                           and c.categories_id = cd.categories_id
                                           order by c.sort_order, cd.categories_name";
								   
          $categories_query = p::query($categories_query);
		  $parent = array();
		  $cat = array();
		  $al = array();
		  while ($categories = p::fetch_array($categories_query, true)) {
		     $parent[ $categories['parent_id'] ][ $categories['categories_id'] ] = array('name' => $categories['categories_name'] ); 
			 
			 $cat[ $categories['categories_id'] ] = array(
	         'name' => $categories['categories_name'], 
			 'parent'=> $categories['parent_id']
	     );

		  }

		  $par = $cat_id;
		  while ( $par != 0){
		      $al[] = $cat[ $par ]['name'];
		      $par = $cat[ $par ]['parent'];
			  
		  }
		  
		 return array_reverse ($al);  
	  }	  
	  
	  public static function remove_categories($ar)
	  {
	     $id = $ar['category_id'];
		 p::query("delete from seo where query='index.php?cat=".(int)$id."'");
	  }
	  	  
	  public static function remove_product($ar)
	  {
	     $id = $ar['products_id'];
		 p::query("delete from seo where query='product_info.php?products_id=".(int)$id."'");
	  }	  
	  	
	  public static function remove_news($ar)
	  {
	     $id = $ar['news_id'];
		 p::query("delete from seo where query='news.php?news_id=".(int)$id."'");
	  }
	  
	  public static function install()
	  {
	       p::query('CREATE TABLE IF NOT EXISTS `seo` (
  `seo_id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`seo_id`),
  KEY `query` (`query`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;;
            ');
			
		   add_option('seo_news', 'news/%name%/');
		   add_option('seo_type_pro', 'seo_type_pro_1', 'radio', "array('seo_type_pro_1', 'seo_type_pro_2', 'seo_type_pro_3')");
		   add_option('seo_type_cat', 'seo_type_cat_1', 'radio', "array('seo_type_cat_1', 'seo_type_cat_2', 'seo_type_cat_3')");
		   add_option('seo_redirect', 'false', 'radio', "array('false', 'true')");
		   
		   add_option('seo::button', '', 'readonly');
		   add_option('seo::button2', '', 'readonly');
	  }
	  
	  public static function remove()
	  {
	     p::query('DROP TABLE seo;');
	  }
	  
	  //кнопка. ссылка на чпу урл генератор
	  public static function button()
	  {
	     echo '<br><a href="plugins.php?main_page=seo::generator">Генератор ЧПУ для всех страниц</a><br>';
	  }	  
	  
	  //кнопка. ссылка на чпу урл генератор
	  public static function button2()
	  {
	     echo '<br><a href="plugins.php?main_page=seo::remove_all">Удаление ЧПУ для всех страниц</a><br><br>';
		 
	  }
	  	  
	  public static function get_seo()
	  {
	    // if ( !is_array(seo::$url))
		 //{
		   seo::$url = array();
		   $a =  p::query('select * from seo where type=""');
		   
		   while ($b = p::fetch_array($a, true)) {
		       seo::$url[ $b['keyword']] = $b['query']; 
		   }
		   
		 //}
	  }
	  
   }
?>