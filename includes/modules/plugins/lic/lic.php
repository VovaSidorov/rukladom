<?php
    /*
    Plugin Name: Сертификаты 
    Plugin URI: http://160.by/
    Version: 1.2
    Author: Евгений Фоминов
    Author URI: http://160.by/
    */
	
    add_filter('menu_modules',    'lic::menu');
    add_action('main_page_admin', 'lic::page');
    add_filter('main_assign',     'lic::info');

    class lic
	{
	   public static function page()
	   {
	      include('lic.page.php');
	   }
	   
	   public static function remove()
	   {
	      p::query("drop table lic;");
	   }
	   
	   public static function install()
	   {
	        p::query("CREATE TABLE IF NOT EXISTS lic (
            lic_id int NOT NULL auto_increment,
            lic_url varchar(255) NOT NULL,
            lic_img varchar(255) NOT NULL,
            lic_sort int NOT NULL,
            language_id int NOT NULL,
            lic_title varchar(255) NOT NULL,
            lic_text1 varchar(255) NOT NULL,
            lic_text2 varchar(255) NOT NULL,
            lic_group varchar(255) NOT NULL,
            PRIMARY KEY  (lic_id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;");

        add_option('lic_status', 'true', 'radio', 'array("true", "false")');
	   }
	   
	   public static function menu($arr)
	   {
	      $arr[] =  array(
            'code' => 'lic',
            'title' => 'Сертификаты',
            'link' => 'plugins.php?main_page=lic::page'
           ); 
		   
	      return $arr;
	   }   
	   
       public static function info($ar)
       {
	       $group = array();

           $s = p::query('select * from lic');
           while ($n = p::fetch_array($s))
           {
			     $group []  = $n;

           } 

		   if ( is_array($group) && count($group) > 0)
		   {
			     $ar[ 'ser' ] =$group ; 
		   }
         
           return $ar;		   
       }
	   
	
	   public static function run()
	   {

	       include('lic.run.php');
	   }
	}



?>