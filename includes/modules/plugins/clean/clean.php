<?php
/*
Plugin Name: Расчет дома
Plugin URI: http://160.by/
Version: 1.0
Author: Евгений Фоминов
Author URI: http://160.by/
*/            

add_action('main_page_admin', 'clean::page');
add_filter('menu_modules', 'clean::menu');
add_filter('product_info_assign', 'clean::info');

class clean
{
    public static function menu($ar)
    {
        $ar[] =  array(
            'code' => 'st',
            'title' => 'Расчет дома',
            'link' => 'plugins.php?main_page=clean::page'
        ); 


        return $ar;
    }          

    public static function page()
    {
        include('clean.page.php');
    }
} 

?>