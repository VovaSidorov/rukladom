<?php
    /*
    Plugin Name: Картинки. История компании
    Plugin URI: http://160.by/
    Version: 1.2
    Author: Евгений Фоминов
    Author URI: http://160.by/
    */
	
    add_filter('menu_modules',    'pic::menu');
    add_action('main_page_admin', 'pic::page');
    add_filter('main_assign',     'pic::info');

    class pic
	{
	   public static function page()
	   {
	      include('pic.page.php');
	   }
	   
	   public static function remove()
	   {
	      p::query("drop table pic;");
	   }
	   
	   public static function install()
	   {
	        p::query("CREATE TABLE IF NOT EXISTS pic (
            pic_id int NOT NULL auto_increment,
            pic_url varchar(255) NOT NULL,
            pic_img varchar(255) NOT NULL,
            pic_sort int NOT NULL,
            language_id int NOT NULL,
            pic_title varchar(255) NOT NULL,
            pic_text1 varchar(255) NOT NULL,
            pic_text2 varchar(255) NOT NULL,
            pic_group varchar(255) NOT NULL,
            PRIMARY KEY  (pic_id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;");

        add_option('pic_status', 'true', 'radio', 'array("true", "false")');
	   }
	   
	   public static function menu($arr)
	   {
	      $arr[] =  array(
            'code' => 'pic',
            'title' => 'Картинки о компании',
            'link' => 'plugins.php?main_page=pic::page'
           ); 
		   
	      return $arr;
	   }   
	   
       public static function info($ar)
       {
	       $group = array();

           $s = p::query('select * from pic');
           while ($n = p::fetch_array($s))
           {
			     $group []  = $n;

           } 

		   if ( is_array($group) && count($group) > 0)
		   {
                 $con = round(count($group)/2);;
                 $pic = array();
                 $pic1 = array();
                 
                 $i=1;
                 foreach ( $group as $id => $val)
                 {
                     
                     if ( $con < $i )  $pic[] = $val; else  $pic1[] = $val;
                     $i++;
                 }
                 $ar[ 'pic' ] = $pic1 ; 
			     $ar[ 'pic1' ] = $pic ; 
		   }
         
           return $ar;		   
       }
	   
	
	   public static function run()
	   {

	       include('pic.run.php');
	   }
	}



?>