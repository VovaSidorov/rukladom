<?php
/* -----------------------------------------------------------------------------------------
$Id: split_page_results.php 1166 2007-02-06 20:23:03 VaM $ 

VaM Shop - open source ecommerce solution
http://vamshop.ru
http://vamshop.com

Copyright (c) 2007 VaM Shop
-----------------------------------------------------------------------------------------
based on: 
(c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
(c) 2002-2003 osCommerce(split_page_results.php,v 1.14 2003/05/27); www.oscommerce.com 
(c) 2003	 nextcommerce (split_page_results.php,v 1.6 2003/08/13); www.nextcommerce.org
(c) 2004	 xt:Commerce (split_page_results.php,v 1.6 2003/08/13); xt-commerce.com

Released under the GNU General Public License 
---------------------------------------------------------------------------------------*/

class splitPageResults {
    var $sql_query, $number_of_rows, $current_page_number, $number_of_pages, $number_of_rows_per_page;

    // class constructor
    function splitPageResults($query, $page, $max_rows, $count_key = '*') {
        $this->sql_query = $query;

        if (empty($page) || (is_numeric($page) == false)) $page = 1;
        $this->current_page_number = $page;

        $this->number_of_rows_per_page = $max_rows;

        $pos_to = strlen($this->sql_query);
        $pos_from = strpos($this->sql_query, ' FROM', 0);

        $pos_group_by = strpos($this->sql_query, ' GROUP BY', $pos_from);
        if (($pos_group_by < $pos_to) && ($pos_group_by != false)) $pos_to = $pos_group_by;

        $pos_having = strpos($this->sql_query, ' HAVING', $pos_from);
        if (($pos_having < $pos_to) && ($pos_having != false)) $pos_to = $pos_having;

        $pos_order_by = strpos($this->sql_query, ' ORDER BY', $pos_from);
        if (($pos_order_by < $pos_to) && ($pos_order_by != false)) $pos_to = $pos_order_by;

        if (strpos($this->sql_query, 'DISTINCT') || strpos($this->sql_query, 'GROUP BY')) {
            $count_string = 'DISTINCT ' . vam_db_input($count_key);
            //$count_string = vam_db_input($count_key);
        } else {
            $count_string = vam_db_input($count_key);
        }

        $count_query = vamDBquery($query);
        $count = vam_db_num_rows($count_query,true);

        $this->number_of_rows = $count;
        $this->number_of_pages = ceil($this->number_of_rows / $this->number_of_rows_per_page);

        if ($this->current_page_number > $this->number_of_pages) {
            $this->current_page_number = $this->number_of_pages;
        }

        $offset = ($this->number_of_rows_per_page * ($this->current_page_number - 1));

        $this->sql_query .= " LIMIT " . max($offset, 0) . ", " . $this->number_of_rows_per_page;
        

    }




    // class functions

    // display split-page-number-links
    function display_links($max_page_links, $parameters = '') 
    {
        $max_page_links = 10;
        function _request_URI() {
            if(!isset($_SERVER['REQUEST_URI'])) {
                $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
                if($_SERVER['QUERY_STRING']) {
                    $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
                }
            }
            return $_SERVER['REQUEST_URI'];
        }
        $rr =  _request_URI();

        $rr =  explode('?', $rr);
        $PHP_SELF =  $rr[0];
        global $request_type;

        $display_links_string = '<div class="block-pages">
                      <hr class="line-page">
                      <div class="row">';

        $class = 'class="pageResults"';
        /*

        <div class="col-xs-2">
        <a href="#"><i class="icon-arrow-left"></i></a>
        </div>
        <div class="col-xs-8">
        <ul class="list-pages">
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a class="active" href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">6</a></li>
        <li><a href="#">7</a></li>
        <li><a href="#">8</a></li>
        <li><a href="#">9</a></li>
        <li><a href="#">10</a></li>
        <li><a href="#">11</a></li>
        <li><a href="#">12</a></li>
        </ul>
        </div>
        <div class="col-xs-2">
        <a href="#"><i class="icon-arrow-right"></i></a>
        </div>

        */
        
        //echo $this->current_page_number;

        if (vam_not_null($parameters) && (substr($parameters, -1) != '&')) $parameters .= '&';

        // previous button - not displayed on first page
        if ($this->current_page_number > 0) { 
            if($this->current_page_number ==1){
                      $display_links_string .= '<div class="col-xs-2 col-sm-2 col-md-2"><a class="page-left" href="' . vam_href_link(basename($PHP_SELF), $parameters . 'p=' . $this->number_of_pages, $request_type) . '"><i class="icon-page"></i></a></div>';
      
           } else {
                $display_links_string .= '<div class="col-xs-2 col-sm-2 col-md-2"><a class="page-left" href="' . vam_href_link(basename($PHP_SELF), $parameters . 'p=' . ($this->current_page_number - 1), $request_type) . '"><i class="icon-page"></i></a></div>';
            }
        }                                    

        $display_links_string .='<div class="col-xs-8 col-sm-8 col-md-8">
                          <ul class="list-page">'; 

        // check if number_of_pages > $max_page_links
        $cur_window_num = intval($this->current_page_number / $max_page_links);
        if ($this->current_page_number % $max_page_links) $cur_window_num++;

        $max_window_num = intval($this->number_of_pages / $max_page_links);
        if ($this->number_of_pages % $max_page_links) $max_window_num++;

        // previous window of pages
        //if ($cur_window_num > 1) $display_links_string .= '<a href="' . vam_href_link(basename($PHP_SELF), $parameters . 'page=' . (($cur_window_num - 1) * $max_page_links), $request_type) . '" class="pageResults" title=" ' . sprintf(PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE, $max_page_links) . ' ">...</a>';

        // page nn button
        for ($jump_to_page = 1 + (($cur_window_num - 1) * $max_page_links); ($jump_to_page <= ($cur_window_num * $max_page_links)) && ($jump_to_page <= $this->number_of_pages); $jump_to_page++) {
            $count = ++$i;
            if ($jump_to_page == $this->current_page_number) {
                $display_links_string .= '<li><a class="active" href="#" onlick="return false;">'.$jump_to_page.'</a></li>';
            } else {
                if( ($count == '1') and ($cur_window_num == '1') ){
                    $display_links_string .= '<li><a href="' . vam_href_link(basename($PHP_SELF), $parameters.'p=' . $jump_to_page, $request_type) . '" >' . $jump_to_page . '</a></li>';
                } else {
                    $display_links_string .= '<li><a href="' . vam_href_link(basename($PHP_SELF), $parameters . 'p=' . $jump_to_page, $request_type) . '">' . $jump_to_page . '</a></li>';
                }
            }
        }

        $display_links_string .=' </ul>
                        </div>'; 
        // next window of pages
        //if ($cur_window_num < $max_window_num) $display_links_string .= '<a href="' . vam_href_link(basename($PHP_SELF), $parameters . 'page=' . (($cur_window_num) * $max_page_links + 1), $request_type) . '" class="pageResults" title=" ' . sprintf(PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE, $max_page_links) . ' ">...</a>&nbsp;';

        // next button    
        if (($this->current_page_number < $this->number_of_pages) && ($this->number_of_pages != 1)) 
        {
           $display_links_string .= '<div class="col-xs-2 col-sm-2 col-md-2"><a class="page-right" href="' . vam_href_link(basename($PHP_SELF), $parameters . 'p=' . ($this->current_page_number + 1), $request_type) . '"><i class="icon-page"></i></a></div>';
        }
        else
        {
      $display_links_string .= '<div class="col-xs-2 col-sm-2 col-md-2"><a class="page-right" href="' . vam_href_link(basename($PHP_SELF), $parameters . 'p=1' , $request_type) . '"><i class="icon-page"></i></a></div>';
               
        }

        $display_links_string .= '  </div>
                    </div>';
        return $display_links_string;
    }

    // display number of total products found
    function display_count() {

        return $this->number_of_pages;
    }
}
?>