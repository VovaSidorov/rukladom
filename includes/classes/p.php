<?php
    /*
    http://160.by/
    Евгений Фоминов
    */
    class p
    {
        static $status = array();
        static $version = '18';
        static $info = array(); 
        static $icons = '';
        static $lang = array();
        static $data = array();
        static $options = array();
        static $_plug_array = array();
        static $var = array(); 
        static $name; //имя текущего плагина
        static $dir; // текущее располодение плагина

        static $module; //$_GET['modules']
        static $type = 'admin';
        static $cms = 'vam';
		
        static $dir_plug = '';
        static $http_plug = '';
		
		/**/
		
		static $p = array();
		static $action = array();
		static $action_plug = array();
		static $filter = array();
		static $filter_name = array();

        public static function plugin_count()
        {
            if (is_array(self::$info) and count(self::$info) > 0)
            {  
                $c = 0;
                foreach (self::$info as $id=>$val)
                {
                    if ($val['status'] == 1) $c++;
                }

                return $c;
            }
            else
            {
                return 0;
            }
        }

        public static function query($query)
        {
            do_action('db_query', array('query'=>$query));
			
            if (self::$cms == 'vam')
            {
                return vam_db_query ($query);
            }
            elseif (self::$cms == 'osc')
            {
                return tep_db_query ($query);
            }
        }	

        public static function perform($table, $data, $action = 'insert', $parameters = '')
        {
            if (self::$cms == 'vam')
            {
                return vam_db_perform ($table, $data, $action, $parameters);
            }
            elseif (self::$cms == 'osc')
            {
                return tep_db_perform ($table, $data, $action, $parameters);
            }
        }	

        public static function fetch_array($query, $act = false)
        {
            if (self::$cms == 'vam')
            {
                return vam_db_fetch_array($query, false);
            }
            elseif (self::$cms == 'osc')
            {
                return tep_db_fetch_array($query, false);
            }
        }			

        public static function insert_id($link = 'db_link')
        {
            if (self::$cms == 'vam')
            {
                return vam_db_insert_id($link);
            }
            elseif (self::$cms == 'osc')
            {
                return tep_db_insert_id($link);
            }
        }			

        public static function num_rows($query, $act = false)
        {
            if (self::$cms == 'vam')
            {
                return vam_db_num_rows($query, $act);
            }
            elseif (self::$cms == 'osc')
            {
                return tep_db_num_rows($query, $act);
            }
        }		

        public static function redirect($url)
        {
            if (self::$cms == 'vam')
            {
                return vam_redirect ($url);
            }
            elseif (self::$cms == 'osc')
            {
                return tep_redirect ($url);
            }
        }		

        public static function prepare_input($query)
        {
            if (self::$cms == 'vam')
            {
                return vam_db_prepare_input ($query);
            }
            elseif (self::$cms == 'osc')
            {
                return tep_db_prepare_input ($query);
            }
        }        
		
		public static function db_input($query)
        {
            if (self::$cms == 'vam')
            {
                return vam_db_input ($query);
            }
            elseif (self::$cms == 'osc')
            {
                return tep_db_input ($query);
            }
        }		

        //Получение информации из языковых файлов
        public static function lang ($name = '')
        { 
            if ( !empty(self::$module) )
            {
                self::$name = self::$module;
                self::set_dir();
            }

            $plugin_dir = DIR_FS_CATALOG.self::$dir;
            
			if ( is_file($plugin_dir.$_SESSION['language'].'.php')){
               require_once($plugin_dir.$_SESSION['language'].'.php');

               if (isset($lang)){
                   self::$lang[self::$name] = $lang;
               }
            }
        }

        public static function desc ($plugin_name)
        {
            $len = 50;

            echo($plugin_name);
        }

        public static function action ()
        {
            $info = '';

            if (self::$name == self::$module)
            {   
                echo( '<img src="'. HTTP_CATALOG_SERVER.DIR_WS_CATALOG.DIR_WS_ADMIN.'images/' . 'icon_arrow_right.gif">' );
            }
            else
            {		
                echo( '<a href="' . "plugins.php?". 'module=' . self::$name . '"><img src="'. HTTP_CATALOG_SERVER.DIR_WS_CATALOG.DIR_WS_ADMIN.'images/' . 'icon_info.gif">'.'</a>' );	
            }

            echo($info);
        }

        //сохранение полей 
        public static function save_options ()
        {
            if (!empty($_POST))
            {
                foreach ($_POST as $_name => $_value)
                {
                    $plugins_value = $_value;
                    $plugins_name = $_name;
                    $plugins_key = self::$module;

                    if (!empty($plugins_name))
                    {
                        p::query("UPDATE plugins SET plugins_value='".$plugins_value."' where plugins_key = '".$plugins_key."' and plugins_name='".$plugins_name."'");
                    }
                }
            }

            return true;
        }

        //вывод статуса в cписки плагина вкл./выкл.
        public static function status ()
        {
            if(isset(self::$info[self::$name]['status']) && (self::$info[self::$name]['status'] ==1))
            {
                echo('<img src="'.HTTP_CATALOG_SERVER.DIR_WS_CATALOG.DIR_WS_ADMIN.'images/' . 'icon_status_green.gif">'.'&nbsp;&nbsp;<a onclick="return confirm(\'Действительно хотите удалить плагин?\')" href="'."plugins.php".'?action=remove&module='.self::$name.'"><img src="'.HTTP_CATALOG_SERVER.DIR_WS_CATALOG.DIR_WS_ADMIN.'images/'. 'icon_status_red_light.gif"></a>');
            }
            else
            {
                echo('<a href="'."plugins.php".'?action=install&module='.self::$name.'"><img src="'.HTTP_CATALOG_SERVER.DIR_WS_CATALOG.DIR_WS_ADMIN.'images/'.'icon_status_green_light.gif">'. '</a>&nbsp;&nbsp;<img src="' .HTTP_CATALOG_SERVER.DIR_WS_CATALOG.DIR_WS_ADMIN.'images/'. 'icon_status_red.gif">');

            }

            return true;
        }

        //вывод опций текущего плагина
        public static function option ()
        {
            echo('<table class="contentTable2" border="0" width="100%" cellspacing="0" cellpadding="0"><tr><td class="infoBoxContent2">');

            self::lang( self::$module );

            echo('<table width="100%" border="0" cellspacing="0" cellpadding="2" style="margin-top:5px;margin-bottom:5px;">');

            if( self::$info[self::$module]['status'] == 1 )
            {
                echo('<form action="'."plugins.php".'?action=save&module='.self::$module.'" method="post">');

                $option = self::get_option(self::$module);

                if (!empty($option))
                {
                    //вывод опций

                    foreach ($option as $option_name => $option_values)
                    {
                        if (!empty($option_values['use_function']))
                        {
                            $_func = 'self::'.$option_values['use_function'];

                            if (isset( self::$lang [self::$module][$option_name]))
                            {
                                $_option_name = Htmlspecialchars( self::$lang [self::$module][$option_name]);

                                echo('<tr><td><b>'.$_option_name.'</b></td></tr>');
                            }

                            $_value_name = trim($option_name.'_desc');
                            if (isset( self::$lang [self::$module][ $_value_name ]))
                            { 
                                $_option_name_desc = addslashes(Htmlspecialchars(self::$lang [self::$module][ $_value_name ]));
                                echo('<tr><td><i>'.$_option_name_desc.'</i></td></tr>');
                            }

                            eval($_func.'\''.Htmlspecialchars($option_values['value']).'\''.', \''.addslashes($option_name)."');");
                            echo('<tr><td></td></tr>');
                            $_option_name = '';
                        }
                    }
                }

                if ($option['group']) unset($option['group']);				

                if (isset(self::$info[self::$module]['process']) && count(self::$info[self::$module]['process'])>0)
                {
                    echo('<tr><td align="center"><a class="button" href="'."plugins.php".'?module='.self::$module.'&action=process"><span>'.PLUGINS_PROCESS.'</span></a></td></tr>');

                }


                if (count($option) == 1)
                {
                    foreach ($option as $na=>$_value_tmp)
                    {
                        if ( isset($_value_tmp['use_function']) && $_value_tmp['use_function'] == 'readonly(')
                        {
                            $option = '';
                        }
                    }
                }


                //если нет опций - не выводится кнопка сохранить опции

                if (!empty($option))
                {
                    echo('<tr><td align="center"><span class="button"><button type="submit" value="Сохранить" >Сохранить</button></span></td></tr>');
                }

                echo('</form>');
                //кнопка удалить плагин
                echo('<tr><td align="center"><form onSubmit="return confirm(\'Действительно хотите удалить плагин?\')" action="'."plugins.php".'?action=remove&module='.self::$module.'" method="post" name="plugin_delete"><span class="button"><button type="submit" value="Удалить">Удалить</button></span></form></td></tr>');		        
            }
            else
            { 

                echo('<table border="0" width="100%">');
                echo('<tr><td align="center"><a class="button" href="'."plugins.php".'?action=install&module='.self::$module.'"><span>Установить</span></a></td></tr>'); 


            }
            echo('</table>');

            echo('</td></tr></table>');

            self::option_desc($_lang_array);

            return true;
        }

        //вывод страницы опций плагина
        public static function option2 ()
        {

            self::lang( self::$module );

            $_lang_array = self::$lang;

            echo('<table width="100%" border="0" cellspacing="0" cellpadding="4" style="margin-top:5px;margin-bottom:5px;">');

            if( self::$info[self::$module]['status'] == 1 )
            {
                echo('<form action="'."plugins.php".'?action=save&module='.self::$module.'" method="post">');



                $option = self::get_option(self::$module, 1);

                if (!empty($option))
                {
                    //вывод опций



                    $_class = 'dataTableContent-odd';
                    foreach ($option as $option_name => $option_values)
                    {
                        if (!empty($option_values['use_function']))
                        {
                            $_func = 'self::'.$option_values['use_function'];

                            if ($_class=='dataTableContent-odd') $_class='dataTableContent-even' ; else $_class='dataTableContent-odd' ;
                            echo '<tr><td class="'.$_class.'" width="300px">';
                            if (isset($_lang_array[self::$module][$option_name]))
                            {
                                $_option_name = Htmlspecialchars($_lang_array[self::$module][$option_name]);

                                echo($_option_name);
                            }
                            echo '</td>';

                            echo '<td class="'.$_class.'">';

                            echo '<table>';
                            eval($_func.'\''.Htmlspecialchars($option_values['value']).'\''.', \''.addslashes($option_name)."');");
                            echo '</table>';

                            echo('</td></tr>');
                            $_value_name = trim($option_name.'_desc');

                            if (isset($_lang_array[self::$module][ $_value_name ]))
                            { 
                                $_option_name_desc = addslashes(Htmlspecialchars($_lang_array[self::$module][ $_value_name ]));
                                echo('<tr><td align="left" colspan="2"><i>'.$_option_name_desc.'</i></td></tr>');
                            }

                            $_option_name = '';
                        }
                    }
                }




                //если нет опций - не выводится кнопка сохранить опции

                if (!empty($option))
                {
                    echo('<tr><td align="center" colspan="2"><span class="button"><button type="submit" value="Сохранить" >Сохранить</button></span></td></tr>');
                }

                echo('</form>');

            }

            echo('</table>');


            return true;
        }

        public static function option_desc($_lang_array)
        {
            $_content_start = '<table class="contentTable4" border="0" width="237px" cellspacing="0" cellpadding="2" style="margin-top:10px;"><tr ><td class="dat1">Описание</td></tr></table>';		
            $_content_start .= '<table class="contentTable2" border="0" width="100%" cellspacing="0" cellpadding="2"><tr><td class="infoBoxContent2">';
            $_content_start .= '<table width="100%" border="0">';
            //описание модуля

            $_content = '';

            if (isset(self::$info[self::$module]['title']) && !empty(self::$info[self::$module]['title']))
            {
                $_content .= '<tr><td align="left"><b>Название</b>: '.self::$info[self::$module]['title'].'</td></tr>';		
            }

            if (isset(self::$info[self::$module]['version']) && !empty(self::$info[self::$module]['version']))
            {
                $_content .= '<tr><td align="left"><b>Версия</b>: '.self::$info[self::$module]['version'].'</td></tr>';		
            }

            if (!empty(self::$info[self::$module]['desc']))
            {
                $_content .= '<tr><td align="left"><b>Краткое описание:</b></td></tr>';	
                $_content .= '<tr><td align="center">'.self::$info[self::$module]['desc'] .'</td></tr>';		
            }

            if (isset($_lang_array[self::$module][self::$module.'_desc']))
            {
                $_content .= '<tr><td align="left"><b>Описание:</b></td></tr>';	
                $_content .= '<tr><td align="center">'.$_lang_array[self::$module][self::$module.'_desc'] .'</td></tr>';		
            }

            if (isset(self::$info[self::$module]['author']) && !empty(self::$info[self::$module]['author']))
            {
                if (isset(self::$info[self::$module]['author_uri']) && !empty(self::$info[self::$module]['author_uri']))
                {
                    $_content .= '<tr><td align="left"><b>Автор</b>: <a href="'.self::$info[self::$module]['author_uri'].'" class="author_uri" target="_blank">'.self::$info[self::$module]['author'].'</a></td></tr>';	
                }	
                else
                {
                    $_content .= '<tr><td align="left"><b>Автор</b>: '.self::$info[self::$module]['author'].'</td></tr>';	
                }				
            }

            $_contentechond = '</table>';	
            $_contentechond .= '</td></tr></table>';	

            if (!empty($_content))
            {
                echo($_content_start);
                echo($_content);
                echo($_contentechond);
            }

        }

        public static function get_option ($key, $type = 0)
        {
            $options = array();

            $plugins_query = p::query('select plugins_key, plugins_name, plugins_value, sort_order, use_function from plugins where plugins_key=\''.$key.'\' and plugins_name <> \'show\' and type="'.(int)$type.'" order by sort_order');

            while ($plugins_result = p::fetch_array($plugins_query))  
            {
                $options[$plugins_result['plugins_name']]['value'] = $plugins_result['plugins_value'];
                $options[$plugins_result['plugins_name']]['use_function'] = $plugins_result['use_function'];
            } 

            return $options;
        }

        public static function remove($_plugin_name = '')
        {
            //определяем имя устанавливаемого плагина
            if (!empty($_plugin_name)) self::$name = $_plugin_name;
            else self::$name = self::$module;


            if (is_file(self::$dir_plug.self::$name.'/'.self::$name.'.php')) 
            {
                $_plug_file = self::$dir_plug.self::$name.'/'.self::$name.'.php';
            }
            elseif (is_file(self::$dir_plug.self::$name.'.php'))
            {
                $_plug_file = self::$dir_plug.self::$name.'.php';
            }
            else break;

            require_once ($_plug_file);
            if ( class_exists (self::$name))
            {
                if ( method_exists(self::$name, 'remove'))
                {
                    $result = call_user_func(array( self::$name, 'remove'));
                }
            }

            if (function_exists(self::$name.'_remove'))
            {
                $func = self::$name.'_remove';
                $func();
            }

            p::query('delete from plugins where plugins_key=\''.self::$name.'\'');

        }

        public static function install ($_plugin_name = '' )
        { 
            //определяем имя устанавливаемого плагина
            if (!empty($_plugin_name)) self::$name = $_plugin_name;  else self::$name = self::$module;


            if (is_file(self::$dir_plug.self::$name.'/'.self::$name.'.php')) 
            {
                $_plug_file = self::$dir_plug.self::$name.'/'.self::$name.'.php';
            }
            elseif (is_file(self::$dir_plug.self::$name.'.php'))
            {
                $_plug_file = self::$dir_plug.self::$name.'.php';
            }
            else break;

            require_once ($_plug_file);

            if ( class_exists (self::$name))
            {
                if ( method_exists(self::$name, 'install'))
                {
                    $result = call_user_func(array( self::$name, 'install'));
                }
            }
            if (function_exists(self::$name.'_install'))
            {
                $func = self::$name.'_install';
                $func();
            }

            p::query("insert plugins (plugins_key, plugins_name, plugins_value, sort_order, sort_plugins, use_function) VALUES ('".self::$name."', 'show','1',0,0, NULL);");
        }

        public static function process ($_plugin_name = '')
        {
            //определяем имя устанавливаемого плагина
            if (!empty($_plugin_name)) self::$name = $_plugin_name;
            else self::$name = self::$module;

            if (!isset(self::$info[self::$name]['process']) or count(self::$info[self::$name]['process'])==0)  return 0;


            self::set_dir();

            require_once (DIR_FS_CATALOG.self::$dir.self::$name.'.php');

            $_pocess = self::$info[self::$name]['process'][0];

            if (function_exists($_pocess))
            {
                $func = $_pocess;
                $func();
            }

            self::redirect("plugins.php".'?module='.self::$name);  		
        }

        public static function set_dir ()
        {

            if (is_file(self::$dir_plug.self::$name.'/'.self::$name.'.php')) 
            {
                self::$dir = 'includes/modules/plugins/'.self::$name.'/';
            }
            elseif (is_file(self::$dir_plug.self::$name.'.php'))
            {
                self::$dir = 'includes/modules/plugins/';
            }

        }

        public static function get_name () //возвращает список плагинов
        {
            $directory_array = array();

            if ($dir = @dir( self::$dir_plug )) 
            {
                while ($file = $dir->read())
                {  
                    if ($file != '.' && $file != '..' && $file != '.svn') 
                    {
                        if (is_dir( self::$dir_plug.$file )) 
                        {
                            if (is_file( self::$dir_plug .$file.'/'.$file.'.php'))
                            {
                                $directory_array[] = array(
                                    0 => $file,
                                    1 =>  self::$dir_plug .$file.'/'.$file.'.php',
                                    2 =>  self::$http_plug.$file.'/',
                                );
                            }
                        }
                        else
                        {
                            if(is_file(self::$dir_plug.$file) && substr($file,count($file)-5,4) == '.php') 
                            {
                                $directory_array[] = array(
                                    0 => substr($file, 0, count($file)-5),
                                    1 => self::$dir_plug .$file,
                                    2 => ''
                                );
                            }
                        }

                    }
                }

                $dir->close();
            }

            sort( $directory_array );

            return $directory_array;
        }

        public static function plugins_install()
        {
            $table = p::query('SHOW TABLES');
            $table_array = array();
            while ($_table = p::fetch_array($table,true))
            {
                $table_array[] =current($_table); 
            }

            if ( !in_array('plugins', $table_array) )
            {
                p::query('CREATE TABLE IF NOT EXISTS `plugins` (
                    `plugins_id` int(11) NOT NULL AUTO_INCREMENT,
                    `plugins_key` varchar(64) NOT NULL,
                    `plugins_name` varchar(64) NOT NULL,
                    `plugins_value` text NOT NULL,
                    `sort_order` int(5) DEFAULT NULL,
                    `sort_plugins` int(5) DEFAULT NULL,
                    `use_function` text,
                    `type` int(1) NOT NULL,
                    PRIMARY KEY (`plugins_id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=103 ;'); 

            }

           /* $s =  p::query('SHOW COLUMNS FROM admin_access');
            $field = array();
            while ($_field = p::fetch_array($s, true))
            {
                $field[] = $_field['Field'];
            } 

            if ( !in_array('plugins', $table_array) )
            {
                p::query('ALTER TABLE `admin_access` ADD `plugins` INT(1) NOT NULL');
                p::query('update admin_access set plugins=1');
            }  */ 
        }

        public static function plugins ($param = '')
        { 
		    self::$dir_plug = DIR_FS_CATALOG.'includes/modules/plugins/';
		    self::$http_plug = HTTP_CATALOG_SERVER.DIR_WS_CATALOG.'includes/modules/plugins/';
			
            if (self::$cms == 'vam')
            {
              //  self::plugins_install();
            }

            $plugins_query = p::query('select plugins_key, plugins_name, plugins_value from plugins order by sort_order');

            while ($plugins_result = p::fetch_array($plugins_query,false))  
            {
                
                if (!empty($plugins_result['plugins_key']))
                {   
                    //определяем все статусы для плагинов
                    if ($plugins_result['plugins_name'] == 'show')
                    {
                        //статус плагина
                        if ((int)$plugins_result['plugins_value'] == 1)
                        {
                            self::$info[$plugins_result['plugins_key']]['status'] = 1;  
                        }
                        else
                        {
                            self::$info[$plugins_result['plugins_key']]['status'] = 0;
                        }
                    }
                    else
                    {
                        self::$options[$plugins_result['plugins_name']] = $plugins_result['plugins_value'];
                    }
                }
            }

			if ( is_file('templates/'.CURRENT_TEMPLATE.'/functions.php'))
            {
                include_once('templates/'.CURRENT_TEMPLATE.'/functions.php');
            }
			
            return true;
        }

        public static function textarea ($value, $name)
        { 
            echo ('<tr><td><textarea class="round plugin" name="'.$name.'" cols="26" rows="10">'.$value.'</textarea></td></tr>');
        }   

        public static function readonly ($value, $name)
        { 
            $func = trim($name.'_readonly');
            $text = '';
			$pa = explode('::', $name); 
			
			if ( count( $pa ) == 2 )
            {
                if ( class_exists ($pa[0]))
                {
                    if ( method_exists($pa[0], $pa[1]))
                    {
					    $text .= '<tr><td>';
						$result = call_user_func(array( $pa[0], $pa[1]));
						$text .= '</td></tr>';
                    }
                }
            }
            else
            {
                if (function_exists($func))
                {
				    $text .= '<tr><td>';
                    $_page(); 
					$text .= '</td></tr>';
                }
            }
        }

        public static function input ($value, $name)
        {
            $value = addslashes($value);
            $size = 15;
            if (mb_strlen($value) > 15) $size = 25;

            echo ('<tr><td><input size="'.$size.'" class="round plugin" type="text" name="'.$name.'" value="'.$value.'"></td></tr>');
        }	

        public static function radio ($_array, $value, $name)
        {
            $_lang_array = self::$lang;

            echo('<tr><td>');

            if (!empty($_array) && is_array($_array))
            {
                foreach ($_array as $_val)
                {
                    $__val = '';
                    //локализация поля
                    if (isset($_lang_array[self::$module][$_val])) $__val = $_lang_array[self::$module][$_val]; else $__val  = $_val;
                    if (strtolower(trim($_val)) == 'true') $__val = YES;
                    if (strtolower(trim($_val)) == 'false') $__val = NO;

                    if ($_val == $value)
                    {
                        echo('<input type="radio" name="'.$name.'" checked value="'.$_val.'"> <b>'.$__val.'</b><br>');
                    }
                    else
                    {
                        echo('<input type="radio" name="'.$name.'" value="'.$_val.'"> <b>'.$__val.'</b><br>');	
                    }
                }
            }

            echo('</td></tr>');

            return true;
        }	

        public static function checkbox ($_array, $value, $name)
        {
            $_lang_array = self::$lang;
            echo('<tr><td>');

            if (!empty($_array) && is_array($_array))
            {
                echo('<select class="round plugin" name="'.$name.'">');
                foreach ($_array as $_val)
                {
                    $__val = '';
                    if ($_val == 'true') $__val = YES;
                    if ($_val == 'false') $__val = NO;

                    //локализация поля
                    if (isset($_lang_array[self::$module][$_val])) 
                    {  
                        $__val = $_lang_array[self::$module][$_val]; 
                    }
                    else
                    {
                        $__val  = $_val;
                    }

                    if ($_val == $value)
                    {
                        echo('<option selected value="'.$_val.'">'.$__val.'</option>');
                    }
                    else
                    {
                        echo('<option value="'.$_val.'">'.$__val.'</option>');	
                    }
                }
                echo('</select>');
            }

            echo('</td></tr>');

        }	

        public static function plug_array ( $themes = '' )
        {
            $plugins_file = self::get_name ();

            $color = '';

            self::$_plug_array = array();

            for ($i = 0, $n = sizeof($plugins_file); $i < $n; $i++) 
            {
                self::$name = $plugins_file[$i][0];

                $plugin_data = get_plugin_data($plugins_file[$i][1]);

                self::set_dir();


                self::$info[self::$name]['title'] = $plugin_data['Title'];

                self::$info[self::$name]['desc'] = $plugin_data['Description'];
                self::$info[self::$name]['author'] = $plugin_data['Author'];
                self::$info[self::$name]['author_uri'] = $plugin_data['AuthorURI'];

                self::$_plug_array[$i]['title'] = $plugin_data['Title'];
                self::$_plug_array[$i]['desc'] = $plugin_data['Description'];
                self::$_plug_array[$i]['version'] = $plugin_data['Version'];
                self::$_plug_array[$i]['author'] = $plugin_data['Author'];
                self::$_plug_array[$i]['author_uri'] = $plugin_data['AuthorURI'];
                self::$_plug_array[$i]['name'] = self::$name;
                self::$info[self::$name]['version'] = $plugin_data['Version'];

                //если не указан статус плагина, устанавливает статус в 0
                if (!isset(self::$info[self::$name]['status']))  self::$info[self::$name]['status'] = 0;

                $color = $color == '#f9f9ff' ? '#f0f1ff':'#f9f9ff';

                self::$_plug_array[$i]['color'] = $color;

                if (is_file(DIR_FS_CATALOG.self::$dir.'icon.png') && !empty($plugins_file[$i][2]))
                {
                    self::$_plug_array[$i]['icon'] =  '<img width="16px" alt="" height="16px" src="'.$plugins_file[$i][2].'icon.png'.'" border="0" />';
                }
                elseif (is_file(DIR_FS_CATALOG.self::$dir.'icon.gif') && !empty($plugins_file[$i][2]))
                {
                    self::$_plug_array[$i]['icon'] =  '<img width="16px" alt="" height="16px" src="'.$plugins_file[$i][2].'icon.gif'.'" border="0" />';
                }
                else
                {
                    self::$_plug_array[$i]['icon'] = '<img width="16px" alt="" height="16px" src="'.HTTP_CATALOG_SERVER.DIR_WS_CATALOG.'images/plug/plug.png'.'" border="0" />';
                }

            }

            return self::$_plug_array;
        }  

        /* подключение активных плагинов */
        public static function require_plugins()
        {
            if (!empty(self::$info) && count(self::$info) > 0)
            {
                foreach (self::$info as $_name => $_value) //подключение активных модулей
                {
                    self::$name = $_name;
                    if (is_file( self::$dir_plug . self::$name.'/'.self::$name.'.php'))
                    {
                        $_plug_file = self::$dir_plug . self::$name.'/'.self::$name.'.php';
                        //указываем какая папка корневая для плагина
                        self::$dir  = 'includes/modules/plugins/'.self::$name.'/';
                    }		 
                    elseif (is_file(self::$dir_plug.self::$name.'.php'))
                    {
                        $_plug_file = self::$dir_plug.self::$name.'.php';
                        //указываем какая папка корневая для плагина
                        self::$dir  = 'includes/modules/plugins/';
                    }else continue;

                    require_once($_plug_file);

                    self::$info[self::$name]['path'] = self::$dir_plug.self::$name.'/';
                    self::$info[self::$name]['url'] =  self::$http_plug.self::$name.'/';
                }

            }
        }


    }

    /*
    WordPress, wordpress.org
    */
    function get_plugin_data( $plugin_file ) 
    { 
        //class plugin
        global $p;

        $fp = fopen($plugin_file, 'r');
        $plugin_data = fread( $fp, 8192 );
        fclose($fp);

        preg_match( '|Plugin Name:(.*)$|mi', $plugin_data, $name );
        preg_match( '|Plugin URI:(.*)$|mi', $plugin_data, $uri );
        preg_match( '|Version:(.*)|i', $plugin_data, $version );
        preg_match( '|Description:(.*)$|mi', $plugin_data, $description );
        preg_match( '|Author:(.*)$|mi', $plugin_data, $author_name );
        preg_match( '|Author URI:(.*)$|mi', $plugin_data, $author_uri );


        $plugin_data = array(
            'Name' => trim(isset($name[1])?$name[1]:''),
            'Title' => (!empty($name[1]) ? ($name[1]) : (ucfirst(p::$name))), 
            'PluginURI' => trim(isset($uri[1])?$uri[1]:''), 
            'Description' => trim(isset($description[1])?$description[1]:''),
            'Author' => trim(isset($author_name[1])?$author_name[1]:''), 
            'AuthorURI' => trim(isset($author_uri[1])?$author_uri[1]:''), 
            'Version' => trim(isset($version[1])?$version[1]:'')
        );

        return $plugin_data;
    } 

    if ( !function_exists('vam_substr'))
    {
        function vam_substr($str, $start, $len = '', $encoding="UTF-8")
        {
            $limit = strlen($str);
            for ($s = 0; $start > 0;--$start) {
                if ($s >= $limit)
                    break;
                if ($str[$s] <= "\x7F")
                    ++$s;
                else {
                    ++$s; 
                    while ($str[$s] >= "\x80" && $str[$s] <= "\xBF")
                        ++$s;
                }
            }
            if ($len == '')
                return substr($str, $s);
            else
                for ($e = $s; $len > 0; --$len) {
                    if ($e >= $limit)
                        break;
                    if ($str[$e] <= "\x7F")
                    ++$e;
                else {
                    ++$e;
                    while ($str[$e] >= "\x80" && $str[$e] <= "\xBF" && $e < $limit)
                        ++$e;
                }
            }
            return substr($str, $s, $e - $s);
        }

    }

    function add_option( $name, $value, $func = 'input', $_array = '', $type=0)
    {
        global $p; 
        global $_plugin_sort;

        if (empty($func)) $func = 'input';
        if (!empty(p::$name)) {
            if (empty($_array)) {
                if ( isset($_plugin_sort[p::$name]) ) $_plugin_sort[p::$name]++;
                else $_plugin_sort[p::$name] = 1;

                p::query("insert plugins (plugins_key, plugins_name, plugins_value, sort_order, sort_plugins, use_function, type) VALUES ('".p::$name."', '".$name."','".$value."','".$_plugin_sort[p::$name]."','0', '".$func."(', ".$type.");");
            }
            else{
                @ $_plugin_sort[p::$name]++;
                if ($func == 'radio' || $func == 'checkbox') {
                    $_array = p::db_input($_array);
                    p::query("insert plugins (plugins_key, plugins_name, plugins_value, sort_order, sort_plugins, use_function, type) VALUES ('".p::$name."', '".$name."','".$value."','".$_plugin_sort[p::$name]."',0, '".$func."($_array,'".", ".$type.");");
                }
            }
        }	   

        return true;   
    }

    function get_option ($_key, $base = false)
    {
        if (!empty(p::$options) && isset(p::$options[$_key])) {
            return p::$options[$_key];
        }
        else return false;
    }

    function update_option ($_key, $_value)
    {
        $_key = p::prepare_input($_key);
        $_value = p::prepare_input($_value);
        if ( isset( p::$options[$_key] ) ) p::$options[$_key] = $_value; 
        if ($_key != 'show')
        {
            p::query("update `plugins` set plugins_value='".$_value."' where plugins_name='".$_key."';");
        }
        return true;
    }

    function add_action($tag, $function_to_add, $priority = 10) 
    {
        p::$info[ p::$name ][ $tag ][] = $function_to_add;
        p::$action_plug [ $function_to_add ] = p::$name;
        p::$action[ $tag ][ $function_to_add ] = $priority;

        return true;
    }

    //добавление фильтра в плагинах
    function add_filter($tag, $function, $priority = '10')
    {
        p::$filter[ $tag ][ $priority ][] = $function;

        if ( count( p::$filter[ $tag ] ) >1 ) 	ksort(p::$filter[ $tag ]);
        if ( count( p::$filter[ $tag ][ $priority ] ) >1 ) sort( p::$filter[ $tag ][ $priority ] );

        // func_name -> plug_name
        p::$filter_name[ $function ] = p::$name;
        p::$info[ p::$name ][ $tag ][] = $function;
      
        return true;
    }

    function apply_filter ($tag, $value, $var = array())
    {
        if ( isset(p::$filter[ $tag ]) )
        {
            foreach (p::$filter[ $tag ] as $_filter_value)
            {
                foreach ($_filter_value as $_filter_func)
                {
                    $_param = explode('::', $_filter_func); 
                    if ( count( $_param ) == 2 )
                    {
                        if ( class_exists ($_param[0]))
                        {
                            if ( method_exists($_param[0], $_param[1]))
                            {
                                p::$name  = p::$filter_name[ $_filter_func ];
                                p::set_dir(); 

                                if ( is_array($var) and count($var) > 0 )
                                {
                                    $value = array('var'=>$var, 'data'=>$value);
                                    $value = call_user_func(array( $_param[0], $_param[1]), $value);

                                    if ( isset($value['data']))   $value = $value['data'];
                                }
                                else
                                {
                                    $value = call_user_func(array( $_param[0], $_param[1]), $value);
                                }


                            }
                        }
                    }
                    else
                    {
                        if (function_exists( $_filter_func ))
                        { 
                            p::$name  = p::$filter_name[ $_filter_func ];
                            p::set_dir();    

                            if ( is_array($var) and count($var) > 0 )
                            {
                                $value = array('var'=>$var, 'data'=>$value);
                                $value = $_filter_func ($value);

                                if ( isset($value['data']))   $value = $value['data'];
                            }
                            else
                            {
                                $value = $_filter_func ($value);
                            }
                        }
                    }


                }
            }
        }

        return $value;
    }

    function plugurl()
    {
        return HTTP_CATALOG_SERVER.'/'.p::$dir;
    }

    function plug_page( $name )
    {	
        return HTTP_CATALOG_SERVER.'/admin/plugins.php?page='.$name;
    }

    function plug_main_page( $name )
    {	
        return HTTP_CATALOG_SERVER.'/admin/plugins.php?main_page='.$name;
    }

    function plugdir()
    {
        return DIR_FS_CATALOG.p::$dir;
    }

    function do_action ($tag, $var = '')
    {
        if (!empty($tag) && isset(p::$action[$tag]))
        {
            foreach (p::$action[$tag] as $_tag => $priority)
            {
                $_param = explode('::',$_tag); 
                if ( count( $_param ) == 2 )
                {
                    if ( class_exists ($_param[0]))
                    {
                        if ( method_exists($_param[0], $_param[1]))
                        {
                            p::$name  = p::$action_plug[ $_tag ];
                            p::set_dir(); 

                            if ( empty($var))
                            {
                                $result = call_user_func(array( $_param[0], $_param[1]));
                            }
                            else
                            {
                                $result = call_user_func(array( $_param[0], $_param[1]), $var);    
                            }
                        }
                    }
                }
                else
                {
                    if (function_exists($_tag))
                    {
                        p::$name  = p::$action_plug[ $_tag ];
                        p::set_dir(); 
                        if ( !empty($var)) $_tag($var); else $_tag();	  
                    }
                }
            }
        }
    }

    function is_action($tags)
    {
        if (isset(p::$action[$tags]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function _mb_substr($str, $start, $len = '', $encoding="UTF-8")
    {
        $limit = strlen($str);
        for ($s = 0; $start > 0;--$start) {
            if ($s >= $limit)
                break;
            if ($str[$s] <= "\x7F")
                ++$s;
            else {
                ++$s; 
                while ($str[$s] >= "\x80" && $str[$s] <= "\xBF")
                    ++$s;
            }
        }
        if ($len == '')
            return substr($str, $s);
        else
            for ($e = $s; $len > 0; --$len) {
                if ($e >= $limit)
                    break;
                if ($str[$e] <= "\x7F")
                ++$e;
            else {
                ++$e;
                while ($str[$e] >= "\x80" && $str[$e] <= "\xBF" && $e < $limit)
                    ++$e;
            }
        }
        return substr($str, $s, $e - $s);
    }


    function add_button($tag, $name, $value = '')
    {
        if (!empty($name) && !empty($tag))
        {
            switch ($tag)
            {
                case 'page':
                    return '<a class="button" href="'."plugins.php".'?page='.$name.'"><span>'.$value.'</span></a>';

                    break;

                case 'main_page':
                    return '<a class="button" href="'."plugins.php".'?main_page='.$name.'"><span>'.$value.'</span></a>';
                    break;
            }
        }
    }
?>
