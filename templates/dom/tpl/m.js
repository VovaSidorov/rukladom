/* 160.by */


var ru2en = { 
    ru_str : "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя", 
    en_str : ['A','B','V','G','D','E','JO','ZH','Z','I','J','K','L','M','N','O','P','R','S','T',
        'U','F','H','C','CH','SH','SHH',String.fromCharCode(35),'I','','JE','JU',
        'JA','a','b','v','g','d','e','jo','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f',
        'h','c','ch','sh','shh',String.fromCharCode(35),'i','','je','ju','ja'], 
    translit : function(org_str) { 
        var tmp_str = ""; 
        for(var i = 0, l = org_str.length; i < l; i++) { 
            var s = org_str.charAt(i), n = this.ru_str.indexOf(s); 
            if(n >= 0) { tmp_str += this.en_str[n]; } 
            else { tmp_str += s; } 
        } 
        return tmp_str; 
    } 
}

function abc (str)
{
    str = ru2en.translit(str); 
    str = str.toLowerCase();
    //str = str + '.html';

    str = str.replace(/\s+/g, '-' );
    str = str.replace(/[!;$,'":*^%#@\[\]&{}]+/g,"");

    return str;
}   

$(function() {


    
    $('[name=see_mail]').submit(
        function ()
        {            
            if ( $('[name=email]').val() != '' &&  validateEmail( $('[name=email]').val() ) )
            {
                jQuery.ajax({
                    url:     '/page.php?page=mail::send',
                    type:     "POST",
                    data: { mail:   $('[name=email]').val() }, 
                    complete: function(data) 
                    { 
                        result = data.responseText;
                        if ( result !='')
                        {
                            form99_ok(result);    
                        }               
                    }      

                }); 

            }
            else
            {
                $('input.sub-inp').css("border-color", "red");
            }

            return false;
        }
    );    


    $('[name=form4]').submit(
        function ()
        {
            if ( $('[name=name4]').val() != '' && $('[name=phone4]').val() != '')
            {
                jQuery.ajax({
                    url:     '/page.php?page=mail::send4',
                    type:     "POST",
                    data: { name:   $('[name=name4]').val(), phone: $('[name=phone4]').val()}, 
                    complete: function(data) 
                    { 
                        result = data.responseText;
                        if ( result !='')
                        {
                            //$('[name=form4]').html(result);   
                            form4_ok();  
                        }               
                    }      

                }); 

            }
            else
            {   
                if ($('[name=name4]').val() == '')  $('[name=name4]').css("border-color", "red");
                if ($('[name=phone4]').val() == '')  $('[name=phone4]').css("border-color", "red");
            }

            return false;
        }
    );    

    $('[name=form5]').submit(
        function ()
        {
            $('[name=name5] input').attr('style', '');
            if ( $('[name=name5]').val() != '' && $('[name=phone5]').val() != '' && $('[name=email5]').val() != '')
            {
                jQuery.ajax({
                    url:     '/page.php?page=mail::send5',
                    type:     "POST",
                    data: jQuery("[name=form5]").serialize() , 
                    complete: function(data) 
                    { 
                        result = data.responseText;
                        if ( result !='')
                        {
                            $('[name=form5]').html(result);     
                        }               
                    }      

                }); 

            }
            else
            {   
                if ($('[name=name5]').val() == '')  $('[name=name5]').css("border-color", "red");
                if ($('[name=phone5]').val() == '')  $('[name=phone5]').css("border-color", "red");
                if ($('[name=email5]').val() == '')  $('[name=email5]').css("border-color", "red");
            }

            return false;
        }
    );

    $('[name=form7]').submit(
        function ()
        {
            $('[name=name7] input').attr('style', '');
            if ( $('[name=name7]').val() != '' && validateEmail($('[name=email7]').val()) && $('[name=phone7]').val() != '' && $('[name=email7]').val() != '')
            {
                jQuery.ajax({
                    url:     '/page.php?page=mail::send7',
                    type:     "POST",
                    data: jQuery("[name=form7]").serialize() , 
                    complete: function(data) 
                    { 
                        result = data.responseText;
                        if ( result !='')
                        {   
                             $('[name=form7]').hide();    
                             $('.letter h2').append(result); 
                        }               
                    }      

                }); 

            }
            else
            {   
                if ($('[name=name7]').val() == '')  $('[name=name7]').css("border-color", "red");
                if ($('[name=phone7]').val() == '')  $('[name=phone7]').css("border-color", "red");
                if ($('[name=email7]').val() == '' &&  !validateEmail($('[name=email7]').val()) )  $('[name=email7]').css("border-color", "red");
            }

            return false;
        }
    );      


    $('[name=form8]').submit(
        function ()
        {
            $('[name=name8] input').attr('style', '');
            if ( $('[name=name8]').val() != '' && $('[name=phone8]').val() != '' && $('[name=time8]').val() != '')
            {
                jQuery.ajax({
                    url:     '/page.php?page=mail::send8',
                    type:     "POST",
                    data: jQuery("[name=form8]").serialize() , 
                    complete: function(data) 
                    { 
                        result = data.responseText;
                        if ( result !='')
                        {
                            $('[name=form8]').hide();    
                            
                             $('.am1 h2').append(result);
    
                        }               
                    }      

                }); 

            }
            else
            {   
                if ($('[name=name8]').val() == '')  $('[name=name8]').css("border-color", "red");
                if ($('[name=phone8]').val() == '')  $('[name=phone8]').css("border-color", "red");
                if ($('[name=time8]').val() == '')  $('[name=time8]').css("border-color", "red");
            }

            return false;
        }
    );     

    $('[name=send2]').submit(
        function ()
        {
            var err = true;
            $('#send2 input').attr('style', '');

            if ( $('[name=name2]').val() != '')  name =  $('[name=name2]').val(); else {$('[name=name2]').css("border-color", "red");err=false;}
            if ( $('[name=phone2]').val() != '') phone =  $('[name=phone2]').val(); else {$('[name=phone2]').css("border-color", "red");err=false;}
            if ( $('[name=email2]').val() != '' && validateEmail( $('[name=email2]').val() ) ) email =  $('[name=email2]').val(); else {$('[name=email2]').css("border-color", "red");err=false;}

            if ( err )
            {
                jQuery.ajax({
                    url:     '/page.php?page=mail::send2',
                    type:     "POST",
                    data: { mail: email, name:name, phone:phone }, 
                    complete: function(data) 
                    { 
                        result = data.responseText;
                        if ( result !='')
                        {
                            form2_ok();   
                        }               
                    }      

                }); 

            }

            return false;
        }
    );    

    $('[name=send9]').submit(
        function ()
        {
            var err = true;
            $('#send9 input').attr('style', '');

            if ( $('[name=name9]').val() != '')  name =  $('[name=name9]').val(); else {$('[name=name9]').css("border-color", "red");err=false;}
            if ( $('[name=phone9]').val() != '') phone =  $('[name=phone9]').val(); else {$('[name=phone9]').css("border-color", "red");err=false;}
            if ( $('[name=email9]').val() != '' && validateEmail ($('[name=email9]').val()) ) email =  $('[name=email9]').val(); else {$('[name=email9]').css("border-color", "red");err=false;}
            m1 =  jQuery("[name=send10]").serialize();
            m1 = m1 + "&"+jQuery("[name=send9]").serialize();

            if ( err )
            {
                jQuery.ajax({
                    url:     '/page.php?page=mail::send9',
                    type:     "POST",
                    data: m1, 

                    complete: function(data) 
                    { 
                        result = data.responseText;
                        if ( result !='')
                        {
                            form9_ok();    
                        }    
                                   
                    }      

                }); 

            }



            return false;
        }
    );

    $('.f16').click(function ()
        {
            id  = $(this).attr('v');
            if ( $(this).hasClass('active') )
            {
                $(this).removeClass('active');
            }
            else
            {
                $(this).addClass('active');
            }

            return false;
        }
    );

    $('#f21_1, #f21_2').keydown(function(event) 
        {

            // Разрешаем: backspace, delete, tab и escape
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
                // Разрешаем: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) || 
                // Разрешаем: home, end, влево, вправо
                (event.keyCode >= 35 && event.keyCode <= 39)) 
            { 

                return;
            }
            else {
                // Обеждаемся, что это цифра, и останавливаем событие keypress

                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) 
                {  

                    event.preventDefault(); 
                } 
                else
                {
                    a = $('#f21_1').val();
                    if (a == '') a =0;
                    b = $('#f21_2').val();
                    if (b == '') b =0;
                    $('[name=f21]').val(a+'-'+b);
                }
            }

    });

    $('#min, #max, .price-from, .price-to, .area-from, .area-to').keydown(function(event) 
        {

            // Разрешаем: backspace, delete, tab и escape
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
                // Разрешаем: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) || 
                // Разрешаем: home, end, влево, вправо
                (event.keyCode >= 35 && event.keyCode <= 39)) 
            { 

                return;
            }
            else {
                // Обеждаемся, что это цифра, и останавливаем событие keypress

                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) 
                {  

                    event.preventDefault(); 
                } 
            }

    });

    $('[name=filter]').submit(
        function ()
        {
            str = '';
            str = str+'min='+$('[name=min]').val();
            str = str+'&max='+$('[name=max]').val();

            $( "a.f16" ).each(function() {

                if ( $(this).hasClass('active'))
                {                               
                    str = str+'&f16['+$(this).attr('v')+']='+$(this).html();   
                }

            });        


            if ( $('[name=f21]').val() !='' )
            {
                str = str+'&f21='+$('[name=f21]').val();
            }               

            as = 0;
            if ( $('[name=f18]').val() !='' )
            { 
                str = str+'&f18='+$('[name=f18]').val()+'-0'; 
            }            

            document.location.href = '/products_filter.php?'+str;
            return false;
        }

    );


    $("[name=phone2]").mask("+7(999)999-99-99");
    $("[name=phone4]").mask("+7(999)999-99-99");
    $("[name=phone5]").mask("+7(999)999-99-99");
    $("[name=phone7]").mask("+7(999)999-99-99");
    $("[name=phone8]").mask("+7(999)999-99-99");
    $("[name=phone9]").mask("+7(999)999-99-99");
    $("[name=phone12]").mask("+7(999)999-99-99");



    $('form#comform input[type=file]').change(
        function ()
        {
            if ($(this).val() !="")
            {
                var w = document.body.clientWidth; 
                if ( w > 700 )
                {
                    $('.icon-status').hide();
                    $('.icon-status2').hide();
                    $('.icon-status').show();

                    if (  $('#fa1').prop("files").length == 1 &&  $('#fa2').prop("files").length == 1)
                    {
                        $('.icon-status2').show();
                    }

                }

                $('input.submit').addClass('active');  




                if ( $('#fa1').prop("files").length == 1)
                {
                    $('a.a1').hide();
                    $('a.a2').show();
                    $('a.a3').hide();
                    $('a.a4').hide();
                }               

                if ( $('#fa1').prop("files").length == 1 &&  $('#fa2').prop("files").length == 1)
                {
                    $('a.a1').hide();
                    $('a.a2').hide();
                    $('a.a3').show();
                    $('a.a4').hide();
                }                   

                if ( $('#fa1').prop("files").length == 1 &&  $('#fa2').prop("files").length == 1 &&  $('#fa3').prop("files").length == 1)
                {
                    $('a.a1').hide();
                    $('a.a2').hide();
                    $('a.a3').hide();
                    $('a.a4').show();
                }   
            }
        }

    );

    form_file = 0;

    $('form#comform').submit(
        function ()
        {
            if (  form_file == 0)
            {
                $('main section.am2').fadeIn(300);
                $('body').css('overflow','hidden');
                return false;
            }
        }
    );

    $('[name=form12]').submit(
        function ()
        {
            var err = true;
            $('#send12 input').attr('style', '');

            if ( $('[name=name12]').val() != '')  name =  $('[name=name12]').val(); else {$('[name=name12]').css("border-color", "red");err=false;}
            if ( $('[name=phone12]').val() != '') phone =  $('[name=phone12]').val(); else {$('[name=phone12]').css("border-color", "red");err=false;}
            //if ( $('[name=time12]').val() != '') email =  $('[name=time12]').val(); else {$('[name=time12]').css("border-color", "red");err=false;}

            if ( err )
            {
                form_file = 1;
                $('[name=name13]').val( $('[name=name12]').val() );
                $('[name=phone13]').val( $('[name=phone12]').val() );
                $('[name=time13]').val( $('[name=time12_1]').val()+':'+$('[name=time12_2]').val() );

                $('#comform').submit();
                $('main section.am2').fadeOut(300);
                $('body').css('overflow','auto');
            }


            return false;
        }
    );

    $("a.upload-link").click (
        function ()
        {
            $('#inp_file').click('form#comform input[type=file]');
        }
    );

    $('.development .link-dev').click(
        function ()
        {
            document.location.href = '/rat.php';
        }
    );

    $('.klm').click(
        function ()
        {
            document.location.href = '/whyas.html#a'+$(this).attr('v');
        }
    );


    }








)

function onok()
{
    $('.discount').prepend('<center style="color:#ffcf00; opacity: 0.6;margin-left: -25rem; width:50rem;left: 50%; position: relative; font-size:3rem;">Спасибо за обращение</center>');
    $('.discount .container').hide();
    setTimeout('onok_close()', 5000);
}

function onok_close()
{
    $('.discount center').remove();
    $('.discount .container').show();
}

function form2_ok()
{
    $('.excursion').prepend('<center style="color:#000;opacity: 0.6;margin-left: -25rem; width:50rem;left: 50%; position: relative;font-size:3rem;">Спасибо за обращение</center>');
    $('.excursion .container').hide(); 
    setTimeout('form2_close()', 5000);
}

function form2_close()
{
    $('#send2 input').not('.submit').val('');
    $('.excursion center').remove();
    $('.excursion .container').show();
}

function form99_ok(text)
{
    $('.subscription').prepend('<center style="color:#000;opacity: 0.6;margin-left: -35rem; width:70rem;left: 50%; position: relative;font-size:3rem;">'+text+'</center>');
    $('.subscription .container').hide(); 
    setTimeout('form99_close()', 5000);
}

function form99_close()
{
    $('[name=see_mail] input').not('.submit').val('');
    $('.subscription center').remove();
    $('.subscription .container').show();
}

function form9_ok()
{
    $('.ss1').prepend('<center style="color:#000;opacity: 0.6; margin-left: -25rem; width:50rem;left: 50%; position: relative;font-size:3rem;">Спасибо за обращение</center>');
    $('.ss1 .container').hide();
    setTimeout('form9_close()', 5000);
}

function form9_close()
{
    $('#send9 input').not('.submit').val('');
    $('.ss1 center').remove();
    $('.ss1 .container').show();
}


function form4_ok()
{
    $('.questions').prepend('<center style="color:#000;opacity: 0.6; width:30rem;left: 50%; position: relative; margin-left: -25rem; width:50rem;font-size:3rem;">Спасибо за обращение</center>');
    $('.questions .container').hide();
    setTimeout('form4_close()', 5000);
}

function form4_close()
{
    $('[name=form4] input').not('.send').val('');
    $('.questions center').remove();
    $('.questions .container').show();
}

function abba2()
{
    $('main section.letter').fadeOut(300);
    $('body').css('overflow','auto');
    $('[name=form7]').show();
     $('[name=name8]').val('')
     $('[name=phone8]').val('')      
    $('.letter center').remove();
    
}

function abba3()
{
    $('main section.am1').fadeOut(300);
    $('body').css('overflow','auto');
    $('#k8').remove();
    $('[name=form8]').show();
    $('[name=form8] input').val('')
    $('[name=form8] textarea').val('')
    
}



$( document ).ready(
    function ()
    {



        $('#time8_1').click(
            function ()
            {
                $('.hours_call_select').toggle();
            }
        );       

        $('.hours_call_select span').click(
            function ()
            {
                $('#time8_1').val(   $(this).html() );
                $('.hours_call_select').hide();
            }
        );          

        $('.minutes_call_select span').click(
            function ()
            {
                $('#time8_2').val(   $(this).html() );
                $('.minutes_call_select').hide();
            }
        );       

        $('#time8_2').click(
            function ()
            {
                $('.minutes_call_select').toggle();
            }
        );        


        $('#time12_1').click(
            function ()
            {
                $('.hours_call_select2').toggle();
            }
        );       

        $('.hours_call_select2 span').click(
            function ()
            {
                $('#time12_1').val(   $(this).html() );
                $('.hours_call_select2').hide();
            }
        );          

        $('.minutes_call_select2 span').click(
            function ()
            {
                $('#time12_2').val(   $(this).html() );
                $('.minutes_call_select2').hide();
            }
        );       

        $('#time12_2').click(
            function ()
            {
                $('.minutes_call_select2').toggle();
            }
        );


        amm = false;
        if (window.param_start && param_start != '') 
        {
            $('[m='+param_start+']').attr('v', 1);
            amm = true;    
        }

        if (window.param_max && param_max != '')
        {
            amm = true;
            $("[name=max]").val( param_max );
        }

        if (window.param_min && param_min != '')
        {
            amm = true;
            $("[name=min]").val( param_min );
        }

        if (amm == true)
        {
            get_products_filter(); 
        }

        jQuery.each(  $('.dd'),

            function ()
            {
                $(this).attr('val', abc( $(this).attr('val') ) );
            }
        );

        $('.header-nav-mobile').removeClass('hi');

        $("#price_sort").on ('click', 

            function ()
            {
                if (param_sort == 'price') 
                {
                    //  param_sort ='';
                    //  $(this).removeClass('act'); 
                    $('.button-filter').click();
                } 
                else 
                {  
                    param_sort = 'price';

                    $('.sidebar-right .list-filter a').removeClass('act');
                    $(this).addClass('act');
                    $('.button-filter span').html( $(this).html());
                }
                $('#load').show();
                get_products_filter(true);
                $('#load').hide();
                return false;
            }
        );


        $("#price_tech").on ('click', 

            function ()
            {
                if (param_sort == 'price_tech') 
                {
                    //  param_sort ='';  
                    // $(this).removeClass('act'); 
                    $('.button-filter').click();
                }
                else 
                {
                    param_sort = 'price_tech';
                    $('.sidebar-right .list-filter a').removeClass('act');
                    $(this).addClass('act');
                    $('.button-filter span').html( $(this).html());
                }

                $('#load').show();
                get_products_filter(true);
                $('#load').hide();
                return false;
            }
        );        


        $("#price_s").on ('click', 
            function ()
            {
                if (param_sort == 'price_s') 
                {
                    //  param_sort ='';  
                    // $(this).removeClass('act'); 
                    $('.button-filter').click();
                }
                else 
                {
                    param_sort = 'price_s';
                    $('.sidebar-right .list-filter a').removeClass('act');
                    $(this).addClass('act');
                    $('.button-filter span').html( $(this).html());
                }

                $('#load').show();
                get_products_filter(true);
                $('#load').hide();
                return false;
            }
        );


        $('.button-filter').click (
            function ()
            {
                if ( dem == 'desc' )
                {
                    dem = 'asc';
                    $('.link-dropdown i').attr('class', 'icon-filter2');
                }
                else
                {
                    dem = 'desc';
                    $('.link-dropdown i').attr('class', 'icon-filter');
                }  

                get_products_filter(true);
            }
        );


        $('.order-phone').on('click', function(event) {
            event.preventDefault();
            $('main section.am1').fadeIn(300);
            $('body').css('overflow','hidden');
        });


        $('section.am1 .hide-order').on('click', function(event) {
            event.preventDefault();
            $('main section.am1').fadeOut(300);
            $('body').css('overflow','auto');
        });           

        $('section.am2 .hide-order').on('click', function(event) {
            event.preventDefault();
            $('main section.am2').fadeOut(300);
            $('body').css('overflow','auto');
        });       


        if ( $('input.radio').length > 0 )
        {
            jQuery.each(  $('input.radio:checked'),
                function ()
                {

                    id = $(this).attr('id');
                    $('.label-name[for='+id+']').addClass('label-b');
                }
            );
        }

        $('input.radio').change (
            function ()
            {
                id = $(this).attr('id');
                $('.label-name[for='+id+']').addClass('label-b');

            }
        );


        $('#slider').slidesjs({
            width: 900,
            height: 655,
            play: {
                active: false,
                auto: false,
                interval: 4000,
                swap: true
            }
        });

        $('.gallery_button').click(function(){
            $('#gallery').arcticmodal()
        });

        $( window ).resize(function() {
            console.log( $(document).width() ); 
        }); 


        var height_document = $(document).height(); 
        var height_client = document.body.clientHeight; 

//        if(height_document > height_client) {  
//            $('footer').attr('style', '');
//        } 
//        else
//        {
//            $('footer').attr('style', 'position: absolute; bottom: 0; left: 0;');
//        }

        if ( run == '/templates/dom/tpl/catalog.min.js' && kr == '') 
        {
            get_products_filter();
        }
        

});


function get_products_filter(m)
{   
    
    
    all = param_str;
    if ( price_min != '') all =  all + '&min='+price_min*10e5;
    if ( price_max != '') all =  all + '&max='+price_max*10e5;
    if ( param_sort != '') all =  all + '&sort='+param_sort+'&direction='+dem;

    if ( w_min != ''|| w_max != '') 
    {
        if (w_min == '') w_min=0;
        if (w_max == '') w_max=0;
        all =  all + '&f21='+w_min+'-'+w_max;
    }

    s=0;
    jQuery.each(  $('.option-link'),
        function ()
        {
            if ( !$(this).hasClass('disable') )
            {
                all =  all + '&f16['+s+']='+$(this).attr('val2');
                s++;
            } 

        }
    );

    s=0;
    jQuery.each(  $('input.checkbox:checked'),
        function ()
        {
            all =  all + '&f22['+s+']='+encodeURIComponent($(this).attr('val2'));
            s++;
        }
    ); 


    if( typeof ajax_request !== 'undefined' && ajax_request != '')
    {
        ajax_request.abort();
    }   
        console.log("/products_filter.php?"+ all+'&type=ajax');
    $('#load').show();
    ajax_request = $.ajax({
        type: "POST", 
        url: "/products_filter.php?"+ all+'&type=ajax', 
        data: "",
        dataType: "json", 

        success: function(data)
        {  
            console.log(data);
            $('#param_count').html( data.count );
            data_file = data.val;
            if (m == true)
            {
                $('#prod').html(data_file);
            }
        },
        complete: function ()
        {
            $('#load').hide(); 
        },

        error: function(data)
        {

        },
    });

}   

function go_cat()
{
    document.location.href = "/catalog.html";
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}